#Lupimedia Slideshow
Provides a Slideshow and Slideshow field. This is a slideshow module that is dependant on drupal bootstrap themes.

##Requirements
* Drupal 8
* Bootstrap 3.x

##Configuration
###Installation

1.) Download composer package by typing

    " composer require lupimedia/drupal_slideshow "
    
2.) On Drupal go to appearance and install the bootstrap theme and set it to default

3.) Then go into 'extend' and find and install the 'lupimedia_slideshow' module

###Adding the block:

Adding the block inside drupal is as normal, in Drupal go to Structure -> block layout and add the block called 'Slideshow (BS)'.

###Adding images: 

1.) On the Drupal site go to Structure -> content types -> basic page

2.) Add a 'Slide' field and give it the label 'slideshow images'

3.) Save the configuration and make some 'Basic Page' content

4.) Fill out the slideshow fields, clear cache and load / refresh the page.

###Options

1.) Once the module is installed go to configuration -> slideshow settings

2.) From here you can change the slideshows basics settings

3.) Save your configuration and then go to configuration -> performance and clear cache