<?php

/**
 * @file
 * Contains slide_entity.page.inc.
 *
 * Page callback for Slide entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Slide entity templates.
 *
 * Default template: slide_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_slide_entity(array &$variables) {
  // Fetch SlideEntity Entity Object.
  $slide_entity = $variables['elements']['#slide_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
