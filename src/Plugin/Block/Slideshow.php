<?php
namespace Drupal\lupimedia_slideshow\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Image\Image;
use Drupal\lupimedia_slideshow\Entity\SlideEntity;

/**
 * Provides a 'slideshow' Block dependent with bootstrap.
 *
 * @Block(
 *   id = "Slideshow",
 *   admin_label = @Translation("Slideshow"),
 *   category = @Translation("Lupimedia Slideshow (BS)"),
 * )
 */
class Slideshow extends BlockBase
{

    /**
     * {@inheritdoc}
     */
    public function build() {
        /** @var \Drupal\node\Entity\Node $node */
        $node = \Drupal::routeMatch()->getParameter('node');
        if(!$node){return;} // Error accounting encase $node == NULL
        $images = $node->get('field_slideshow_panels')->getValue();
        /**
         * [
         *   'slide' => $fid,
         *   'title' => 'string from editor'
         *   'text' => 'string from editor'
         *   'button_link' => 'string from editor'
         *   'button_text' => 'string from editor'
         * ]
         */
        $slides = [];
        foreach ($images as $image) {
            $entity = SlideEntity::load($image['target_id']);

            $slides[] = [
                'slide' => $entity->getMedia()->field_image->entity->getFileUri(), // Loads from $fid (Field ID) above
                'title' => $entity->getName(),
                'text'  => $entity->getCaption()
            ];
        }

        $config = \Drupal::config('lupimedia_slideshow.settings');
        $options[] = $config->get('_options');

        return [
            '#cache'    => [
                'contexts' => [
                    'url.path'
                ]
            ],
            '#theme'    => 'slideshow',
            '#images'   => $slides, // Passes the variables into twig
            '#config'   => [
                'options' => $options
            ],
            '#attached' => [
                'library' => [
                    'lupimedia_slideshow/slideshow_block',
                ],
            ],
        ];
    }
}