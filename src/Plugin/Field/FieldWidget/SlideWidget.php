<?php
/**
 * @file
 * Contains \Drupal\lupimedia_slideshow\Plugin\Field\FieldWidget\SlideWidget.
 */
namespace Drupal\lupimedia_slideshow\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Slide' widget.
 *
 * @FieldWidget(
 *   id = "SlideWidget",
 *   module = "lupimedia_slideshow",
 *   label = @Translation("Slideshow fields"),
 *   field_types = {
 *     "slide",
 *     "title",
 *     "text",
 *     "button_link",
 *     "button_text",
 *   }
 * )
 */
class SlideWidget extends WidgetBase implements WidgetInterface {

    /**
     * {@inheritdoc}
     */
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
        foreach ($values as $delta => $value) {
            if (count($value['slide'])>0) {
                $values[$delta]['slide'] = (int) $value['slide'][0];
            }
        }
        return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

        $element['slide'] = [
            '#title' => t('Slide Image'),
            '#type' => 'managed_file',
            '#upload_location' => 'public://',
            '#default_value' => '',
            '#states' => [
                'visible' => [
                    ':input[name="File_type"]' => array('value' => t('Upload Your File')),
                ],
            ],
        ];
        $element['title'] = [
            '#title' => t('Slide Title'),
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->title) ? $items[$delta]->title : NULL,
        ];
        $element['text'] = [
            '#title' => t('Slide Text'),
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->text) ? $items[$delta]->text : NULL,
        ];
        $element['button_link'] = [
            '#title' => t('Button Link'),
            '#description' => 'If you do not want a button displayed for this slide, leave this field blank',
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->button_link) ? $items[$delta]->button_link : NULL,
        ];
        $element['button_text'] = [
            '#title' => t('Button Text'),
            '#description' => 'If you do not want a button displayed for this slide, leave this field blank',
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->button_text) ? $items[$delta]->button_text : NULL,
        ];

        if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
            $element += [
                '#type' => 'fieldset',
                '#attributes' => ['class' => ['container-inline']],
            ];
        }

        return $element;
    }
}