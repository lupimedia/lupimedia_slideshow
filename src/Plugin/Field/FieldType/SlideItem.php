<?php
/**
 * @file
 * Contains \Drupal\lupimedia_slideshow\Plugin\Field\FieldType\SlideItem.
 */
namespace Drupal\lupimedia_slideshow\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type : Slide.
 *
 * @FieldType(
 *   id = "slide",
 *   label = @Translation("Slide"),
 *   module = "lupimedia_slideshow",
 *   description = @Translation("Creates field for slideshow"),
 *   default_formatter = "SlideFormatter",
 *   default_widget = "SlideWidget",
 * )
 */
class SlideItem extends FieldItemBase
{

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {

        return [
            'columns' => [
                'slide' => [
                    'description' => 'The ID of the referenced managed file',
                    'type'        => 'int',
                ],
                'title' => [
                    'description' => 'Title for the Slide.',
                    'type'        => 'varchar',
                    'length'      => 100,
                ],
                'text'  => [
                    'description' => 'Slide description / text.',
                    'type'        => 'varchar',
                    'length'      => 100,
                ],
                'button_link' => [
                    'description' => 'Link for the Button.',
                    'type'        => 'varchar',
                    'length'      => 100,
                ],
                'button_text' => [
                    'description' => 'Text for the Button.',
                    'type'        => 'varchar',
                    'length'      => 100,
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

        $properties = [];

        $properties['slide'] = DataDefinition::create('integer')
                                             ->setLabel(t('Slide Image'));

        $properties['title'] = DataDefinition::create('string')
                                             ->setLabel(t('Slide Title'));

        $properties['text'] = DataDefinition::create('string')
                                            ->setLabel(t('Slide Text'));

        $properties['button_link'] = DataDefinition::create('string')
                                            ->setLabel(t('Button Link'));

        $properties['button_text'] = DataDefinition::create('string')
                                            ->setLabel(t('Button Text'));

        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {

        return !($this->get('slide')->getValue()
            || $this->get('title')->getValue()
            || $this->get('text')->getValue()
            || $this->get('button_link')->getValue()
            || $this->get('button_text')->getValue()
        );
    }

}