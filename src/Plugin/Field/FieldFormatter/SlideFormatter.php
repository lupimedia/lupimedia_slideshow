<?php
/**
 * @file
 * Contains \Drupal\lupimedia_slideshow\Plugin\Field\FieldFormatter\SlideFormatter.
 */
namespace Drupal\lupimedia_slideshow\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'Slide' formatter.
 *
 * @FieldFormatter(
 *   id = "SlideFormatter",
 *   label = @Translation("Slide Formatter"),
 *   field_types = {
 *     "slide",
 *     "title",
 *     "text",
 *     "button_link",
 *     "button_text"
 *   }
 * )
 */

class SlideFormatter extends FormatterBase {

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $summary = [];
        $summary[] = t('Slideshow module which displays a block.');

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {

        $element = [];

        foreach ($items as $delta => $item) {
            // Render each element as markup.
            $element[$delta] = [
                '#type' => 'markup',
                '#markup' => $item->value,
            ];
        }

        return $element;
    }

}