<?php

namespace Drupal\lupimedia_slideshow;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Slide entity entity.
 *
 * @see \Drupal\lupimedia_slideshow\Entity\SlideEntity.
 */
class SlideEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\lupimedia_slideshow\Entity\SlideEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished slide entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published slide entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit slide entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete slide entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add slide entity entities');
  }

}
