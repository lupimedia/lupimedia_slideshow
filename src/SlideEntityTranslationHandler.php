<?php

namespace Drupal\lupimedia_slideshow;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for slide_entity.
 */
class SlideEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
