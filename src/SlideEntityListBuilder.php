<?php

namespace Drupal\lupimedia_slideshow;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Slide entity entities.
 *
 * @ingroup lupimedia_slideshow
 */
class SlideEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Slide entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\lupimedia_slideshow\Entity\SlideEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.slide_entity.edit_form',
      ['slide_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
