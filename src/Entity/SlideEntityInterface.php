<?php

namespace Drupal\lupimedia_slideshow\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Slide entity entities.
 *
 * @ingroup lupimedia_slideshow
 */
interface SlideEntityInterface extends  ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Slide entity name.
   *
   * @return string
   *   Name of the Slide entity.
   */
  public function getName();

  /**
   * Sets the Slide entity name.
   *
   * @param string $name
   *   The Slide entity name.
   *
   * @return \Drupal\lupimedia_slideshow\Entity\SlideEntityInterface
   *   The called Slide entity entity.
   */
  public function setName($name);

  /**
   * Gets the Slide entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Slide entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Slide entity creation timestamp.
   *
   * @param int $timestamp
   *   The Slide entity creation timestamp.
   *
   * @return \Drupal\lupimedia_slideshow\Entity\SlideEntityInterface
   *   The called Slide entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Slide entity published status indicator.
   *
   * Unpublished Slide entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Slide entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Slide entity.
   *
   * @param bool $published
   *   TRUE to set this Slide entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\lupimedia_slideshow\Entity\SlideEntityInterface
   *   The called Slide entity entity.
   */
  public function setPublished($published);

}
