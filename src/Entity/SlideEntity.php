<?php

namespace Drupal\lupimedia_slideshow\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Slide entity entity.
 *
 * @ingroup lupimedia_slideshow
 *
 * @ContentEntityType(
 *   id = "slide_entity",
 *   label = @Translation("Slide entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lupimedia_slideshow\SlideEntityListBuilder",
 *     "views_data" = "Drupal\lupimedia_slideshow\Entity\SlideEntityViewsData",
 *     "translation" = "Drupal\lupimedia_slideshow\SlideEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\lupimedia_slideshow\Form\SlideEntityForm",
 *       "add" = "Drupal\lupimedia_slideshow\Form\SlideEntityForm",
 *       "edit" = "Drupal\lupimedia_slideshow\Form\SlideEntityForm",
 *       "delete" = "Drupal\lupimedia_slideshow\Form\SlideEntityDeleteForm",
 *     },
 *     "access" = "Drupal\lupimedia_slideshow\SlideEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lupimedia_slideshow\SlideEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "slide_entity",
 *   data_table = "slide_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer slide entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/slide_entity/{slide_entity}",
 *     "add-form" = "/admin/structure/slide_entity/add",
 *     "edit-form" = "/admin/structure/slide_entity/{slide_entity}/edit",
 *     "delete-form" = "/admin/structure/slide_entity/{slide_entity}/delete",
 *     "collection" = "/admin/structure/slide_entity",
 *   },
 *   field_ui_base_route = "slide_entity.settings"
 * )
 */
class SlideEntity extends ContentEntityBase implements SlideEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

    /**
     * {@inheritdoc}
     */
    public function getCaption() {
        return $this->get('caption')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setCaption($caption) {
        $this->set('caption', $caption);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMedia() {
        return $this->get('media')->first()
                    ->get('entity')
                    ->getTarget()
                    ->getValue()
            ;
    }

    /**
     * {@inheritdoc}
     */
    public function setMedia($media) {
        $this->set('media', $media);
        return $this;
    }



    /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Slide entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Slide entity entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['caption'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Caption'))
          ->setDescription(t('The caption to be displayed in the slideshow.'))
          ->setSettings([
              'max_length' => 50,
              'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => -4,
          ])
          ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => -4,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Slide entity is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    // For the database config for this, look in config table
    // name = "core.entity_form_display.slide_entity.slide_entity.default"
    $fields['media'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Media'))
        ->setDescription(t('The image for the slide'))
        ->setSettings([
            'target_type' => 'media',
            'default_value' => 0,
        ])
        ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'entity_reference_label',
            'weight' => -3,
        ])
        ->setDisplayOptions('form', [
            'type' => 'inline_entity_form_complex',
            'settings' => [
                "form_mode" => "default",
                "allow_new" => true,
                "allow_existing" => true,
                "match_operator" => "CONTAINS"
            ],
            'weight' => -3,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
