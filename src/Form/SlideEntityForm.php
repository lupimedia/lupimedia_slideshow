<?php

namespace Drupal\lupimedia_slideshow\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Slide entity edit forms.
 *
 * @ingroup lupimedia_slideshow
 */
class SlideEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\lupimedia_slideshow\Entity\SlideEntity */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Slide entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Slide entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.slide_entity.canonical', ['slide_entity' => $entity->id()]);
  }

}
