<?php
namespace Drupal\lupimedia_slideshow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure lupimedia_slideshow settings for this site.
 */
class ModuleSettingsForm extends ConfigFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'lupimedia_slideshow_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'lupimedia_slideshow.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('lupimedia_slideshow.settings');

        $form['_options'] = array(
            '#type'          => 'checkboxes',
            '#options'       => array(
                'Indicators'   => $this->t('Indicators'),
                'Controls'     => $this->t('Controls'),
                'Captions'     => $this->t('Captions'),
            ),
            '#title'         => $this->t('Customise your slideshow block.'),
            '#default_value' => $config->get('_options'),
        );

        $form['_height'] = array(
            '#type'          => 'number',
            '#title'         => $this->t('Set the default slideshow height.'),
            '#default_value' => $config->get('_height'),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Retrieve the configuration
        $this->config('lupimedia_slideshow.settings')
             ->set('_options', $form_state->getValue('_options'))
             ->set('_height', $form_state->getValue('_height'))
             ->save();

        parent::submitForm($form, $form_state);
    }
}