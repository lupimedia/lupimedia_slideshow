<?php

namespace Drupal\lupimedia_slideshow\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Slide entity entities.
 *
 * @ingroup lupimedia_slideshow
 */
class SlideEntityDeleteForm extends ContentEntityDeleteForm {


}
