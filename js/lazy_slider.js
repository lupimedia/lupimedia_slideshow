window.addEventListener('load', function(){
    jQuery(function($){
        $('#carousel-example-generic').on('slide.bs.carousel', function(e){
            var currentSlide = $(e.relatedTarget);
            if(currentSlide.find('img').attr('data-src')){
                currentSlide.find('img').attr('src', currentSlide.find('img').attr('data-src')).removeAttr('data-src');
            }

            var nextSlide = currentSlide.next('.item');
            if(nextSlide.find('img').attr('data-src')){
                nextSlide.find('img').attr('src', nextSlide.find('img').attr('data-src')).removeAttr('data-src');
            }
        });

        var secondSlide = $('#carousel-example-generic .item.active').next();
        if(secondSlide.find('img').attr('data-src')){
            secondSlide.find('img').attr('src', secondSlide.find('img').attr('data-src')).removeAttr('data-src');
        }
    });
})